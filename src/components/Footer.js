import React from 'react';

const Footer = ({titulo, fecha}) => {
    return ( 
        <footer>
            <p>Todos los derechos reservados a {titulo} &copy; {fecha}</p>
        </footer>
     );
}

export default Footer;